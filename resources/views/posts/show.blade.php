@extends('layouts.app')

@section('content')
    <div class="col-12 mb-4">
                <div class="card mb-4 box-shadow ">
                        <div class="text-center">
                            <img class=" img-fluid  " src="{{$post->img_path}}" alt="Thumbnail [100%x225]"  width="400">

                        </div>
                    <div class="card-body">
                        <div class="card-header">
                            {{$post->name}}
                        </div>
                        <div class="card-text  mt-4">
                            <div class="h3 pl-4">
                                Post info:
                            </div>
                            <p class="pl-4">
                                {{$post->description}}
                            </p>

                        </div>

                        @auth
                            <hr>
                        <div >
                            <div class="mesgs mt-5">
                                <div class="msg_history">
                                    @foreach($post->comments as $comment)
                                        @if($comment->user_id == auth()->id())
                                            <div class="outgoing_msg">
                                                <div class="sent_msg">
                                                    <p>{{$comment->text}}</p>
                                                    <span
                                                        class="time_date"> {{$comment->created_at->diffForHumans()}}</span>
                                                </div>
                                            </div>

                                        @else
                                            <div class="incoming_msg">
                                                <div>{{$comment->comentor->name}}</div>
                                                <div class="incoming_msg_img"><img
                                                        src="https://ptetutorials.com/images/user-profile.png"
                                                        class="img-fluid"></div>
                                                <div class="received_msg">
                                                    <div class="received_withd_msg">
                                                        <p>{{$comment->text}}</p>
                                                        <span
                                                            class="time_date"> {{$comment->created_at->diffForHumans()}}</span>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif
                                    @endforeach


                                </div>

                            </div>

                            <div class="h4 mt-4"> Write comment </div>
                            <form method="POST"
                                  action="{{route('comments.store', ['id' => auth()->id(), 'post' => $post->id])}}"
                                  class="comment-form"
                            >
                               @csrf
                                <div class="input-group">
                                    <input type="text" name="text" class="form-control " placeholder="Comment" aria-label="Recipient's username" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <button class="btn btn-outline-primary" type="submit">Send</button>
                                    </div>
                                </div>

                            </form>
                        </div>
                        @endauth
                    </div>
                </div>
    </div>

@endsection

@push('scripts')
    <script  defer src="{{asset('js/post.js')}}"></script>
@endpush
