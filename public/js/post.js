$(document).ready(function () {

    $(document).on('submit', '.comment-form' ,( e )=> {
      e.preventDefault()
        let form  = $(e.target)

        $.ajax({
            url: form.attr('action'),
            type:form.attr('method'),
            data : form.serializeArray(),
            success: function (data) {
                let html = $('.msg_history').html();
                $('.msg_history').html(html + data);
            },
            error: function (data) {
               alert(data);
            }
        })
    });

});
