<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\PostController::class, 'list'])->name('posts');
Route::get('posts/{post}', [\App\Http\Controllers\PostController::class, 'show'])->name('posts.show');

Auth::routes(['verify' => true]);


Route::group(['middleware' => ['verified', 'auth'] ], function (    ) {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::resource('users/{id}/posts',    'PostController')->except('show');
    Route::resource('users/{id}posts/{post}/comments',    'CommentController')->only(['update', 'store']);
});
