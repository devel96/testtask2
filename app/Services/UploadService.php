<?php


namespace App\Services;


use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class UploadService
{
    protected $file;

    protected $path = 'uploads';

    public function __construct(UploadedFile $file = null)
    {
        $this->file = $file;
    }

    /**
     * @return false|string
     */
    public function upload()
    {

        $filenameWithExt = $this->file->getClientOriginalName();
        $fileNameToStore = time() . '_' . $filenameWithExt;

        $this->file->storeAs('public/' . $this->path, $fileNameToStore);

        return "$this->path/$fileNameToStore";
    }

    /**
     * @param $path
     * @return bool
     */
    public function delete($path)
    {
        return Storage::delete('public/' . $path);
    }
}
