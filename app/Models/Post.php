<?php

namespace App\Models;

use App\Services\UploadService;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\Relation;

class Post extends Model
{
    /**
     * @var string[]
     */
    protected $fillable = [
      'user_id',
      'name',
      'description',
      'image',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments(): Relation
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author(): Relation
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return string
     */
    public function getImgPathAttribute() : string
    {
        return $this->image ? "/storage/$this->image" : asset('images/default_user.png');
    }

    /**
     * @param null $image
     */
    public function deleteExistingImg($image = null ): void
    {
        $image = $image ?? $this->image;

        if($image) {
            (new UploadService())->delete($image);
        }
    }

    /**
     * Delete old images  from storage
     */
    protected static function boot() {
        parent::boot();

        static::deleted(function($post) {
            $post->deleteExistingImg();
        });

        static::updating(function($post) {
            $path =   $post->getOriginal('image');
            $post->deleteExistingImg($path);
        });
    }

}
