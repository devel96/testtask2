<?php

namespace App\Http\Controllers;

use App\Http\Requests\Post\PostCreateRequest;
use App\Http\Requests\Post\PostUpdateRequest;
use App\Interfaces\PostInterface;
use App\Models\Post;
use App\Services\UploadService;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /**
     * PostController constructor.
     * @param PostInterface $repository
     */
    public function __construct(PostInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function list(Request $request)
    {
        $posts = $this->repository->get();

        if($request->ajax()) {
            return view('components.posts', compact('posts'));
        }

        return view('index', compact('posts'));
    }



    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $posts = $this->repository->userPosts();

        return view('posts.index', compact('posts'));
    }

    /**
     * @param $userId
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create($userId, Request $request)
    {
        return view('posts.create');
    }

    /**
     * @param int $userId
     * @param PostCreateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store( int $userId, PostCreateRequest $request)
    {
        $form = $request->only(['name', 'description', 'image']);
        $this->repository->store($form);

        return redirect()->route('posts.index', $request->user())->with(['success' => "Post Created"]);
    }


    /**
     * @param $userId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function edit(int $userId, Post $post, Request $request)
    {
        $this->repository->edit($post);

        return view('posts.edit' , compact('post'));
    }

    /**
     * @param Post $post
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Post $post)
    {
        $post->load('comments.comentor:name,id');

        return view('posts.show' , compact('post'));
    }

    /**
     * @param $id
     * @param Post $post
     * @param PostUpdateRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(int $id, Post $post, PostUpdateRequest $request)
    {
        $this->repository->update($post, $request->only(['name', 'description', 'image']));

        return redirect()->route('posts.index', $request->user())->with(['success' => "Post Updated"]);
    }

    /**
     * @param $userId
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(int $userId, Post $post, Request $request)
    {
        $post->delete();

        return redirect()->route('posts.index', $request->user())->with(['success' => "Post deleted"]);
    }
}
