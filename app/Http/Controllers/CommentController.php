<?php

namespace App\Http\Controllers;

use App\Http\Requests\Post\PostCommentRequest;
use App\Models\Post;

class CommentController extends Controller
{
    /**
     * @param $id
     * @param Post $post
     * @param PostCommentRequest $request
     */
    public function store($id, Post $post, PostCommentRequest $request)
    {
        $data = array_merge($request->only('text'), ['user_id' => $id]);

        $comment = $post->comments()->create($data);

        return view('components.comment', compact('comment'));
    }
}
