<?php

namespace App\Interfaces;

use App\Models\Post;

interface PostInterface
{
    /**
     * @return mixed
     */
    public function userPosts();

    /**
     * @param Post $post
     * @return mixed
     */
    public function edit(Post $post);

    /**
     * @param Post $post
     * @param array $data
     * @return mixed
     */
    public function update(Post $post, array $data);

}
